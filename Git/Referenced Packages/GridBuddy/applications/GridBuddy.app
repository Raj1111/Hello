<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>All_Grids</defaultLandingTab>
    <description>Multi-object editable grids for Salesforce</description>
    <label>GridBuddy</label>
    <tab>All_Grids</tab>
    <tab>Grid_Wizard</tab>
    <tab>GridBuddy_Setup_Guide</tab>
    <tab>GridBuddy_User_Guide</tab>
    <tab>Buy_GridBuddy</tab>
    <tab>Trail__c</tab>
    <tab>Campsite__c</tab>
    <tab>Campsite_Reservation__c</tab>
</CustomApplication>
